f = open('example_text.txt')
line = f.readline()
lines = f.readlines()

mod_lines = [x.rstrip() for x in lines]

def count_men(instring):
    num = 0
    if instring.__class__ == str:
        num = instring.count('men') + instring.count('Men')
    if instring.__class__ == list:
        for x in instring:
            num = num + count_men(x)
    return num

def capitalize_women(instring):
    capital_wmn = str()
    if instring.__class__ == str:
        capital_wmn = instring.replace('women', 'WOMEN')
        capital_wmn = capital_wmn.replace('Women', 'WOMEN')
    if instring.__class__ == list:
        for x in instring:
            capital_wmn = capital_wmn + '\n' + capitalize_women(x)
    return capital_wmn

def contains_blue_devil(infile):
    has_blue_devil = False
    f = open(infile, 'r')
    for x in f.readlines():
        if "Blue Devil" in x:
            has_blue_devil = True
    return has_blue_devil

def find_non_terminal_said(instring):
    import re
    p = re.compile('said[^.]')
    return p.match(instring)

def cap_multi_vowel_words(instring):
    import re
    p = re.compile([aeiouy]{2,})
    return p.match(instring)
 
